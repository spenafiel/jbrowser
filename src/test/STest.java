package test;

import static org.junit.Assert.assertEquals;


import org.junit.Test;
import sh4j.model.browser.SClass;
import sh4j.model.browser.SMethod;
import sh4j.model.highlight.SClassName;
import sh4j.model.highlight.SCurlyBracket;
import sh4j.model.highlight.SKeyWord;
import sh4j.model.highlight.SMainClass;
import sh4j.model.highlight.SModifier;
import sh4j.model.highlight.SPseudoVariable;
import sh4j.model.highlight.SSemiColon;
import sh4j.model.highlight.SString;
import sh4j.model.style.SBredStyle;
import sh4j.model.style.SDarkStyle;
import sh4j.model.style.SEclipseStyle;
import sh4j.parser.SClassParser;

import java.util.List;

public class STest {

  @Test
  public void penguinTestEclipse() {
    List<SClass> cls=SClassParser.parse("class Penguin extends Animal{ public void sayHello(){\n " +
        "String s = this.getHowl();\n System.out.println(s+\", \"+s+\"!\""
        +"}}");
    SMethod method=cls.get(0).methods().get(0);
    String html= method.body().toHTML(new SEclipseStyle(),new SClassName(),new SCurlyBracket(),new SKeyWord(),new SMainClass(), new SModifier(),new SPseudoVariable(),new SSemiColon(),new SString());
    assertEquals("<pre style='color:#000000;background:#ffffff;'>\n" +
        "<span style='background:#f1f0f0;'>  1 </span>  <span style='color:#7f0055; " +
        "font-weight:bold; '>public</span> <span style='color:#7f0055; font-weight:bold; " +
        "'>void</span> sayHello()<span style='font-weight:bold; '>{</span>\n" +
        "<span style='background:#f1f0f0;'>  2 </span>    \n" +
        "<span style='background:#f1f0f0;'>  3 </span>  <span style='font-weight:bold; " +
        "'>}</span></pre>",html);
  }

  @Test
  public void penguinTestDark() {
    List<SClass> cls=SClassParser.parse("class Penguin extends Animal{ public void sayHello(){\n " +
        "String s = this.getHowl();\n System.out.println(s+\", \"+s+\"!\""
        +"}}");
    SMethod method=cls.get(0).methods().get(0);
    String html= method.body().toHTML(new SDarkStyle(),new SClassName(),new SCurlyBracket(),new SKeyWord()
        ,new SMainClass(), new SModifier(),new SPseudoVariable(),new SSemiColon(),new SString());
    assertEquals("<pre style='color:#d1d1d1;background:#000000;'>\n" +
        "<span style='background:#f1f0f0;'>  1 </span>  <span style='color:#e66170; " +
        "font-weight:bold; '>public</span> <span style='color:#bb7977; '>void</span> sayHello()" +
        "<span style='color:#b060b0; '>{</span>\n" +
        "<span style='background:#f1f0f0;'>  2 </span>    \n" +
        "<span style='background:#f1f0f0;'>  3 </span>  <span style='color:#b060b0; '>}</span></pre>",html);
  }

  @Test
  public void penguinTestBred() {
    List<SClass> cls=SClassParser.parse("class Penguin extends Animal{ public void sayHello(){\n " +
        "String s = this.getHowl();\n System.out.println(s+\", \"+s+\"!\""
        +"}}");
    SMethod method=cls.get(0).methods().get(0);
    String html= method.body().toHTML(new SBredStyle(),new SClassName(),new SCurlyBracket(),new SKeyWord()
        ,new SMainClass(), new SModifier(),new SPseudoVariable(),new SSemiColon(),new SString());
    assertEquals("<pre style='color:#000000;background:#f1f0f0;'>\n" +
        "<span style='background:#f1f0f0;'>  1 </span>  <span style='color:#400000; " +
        "font-weight:bold; '>public</span> <span style='color:#800040; '>void</span> sayHello()" +
        "<span style='color:#806030; '>{</span>\n" +
        "<span style='background:#f1f0f0;'>  2 </span>    \n" +
        "<span style='background:#f1f0f0;'>  3 </span>  <span style='color:#806030; '>}</span></pre>",html);
  }

}
