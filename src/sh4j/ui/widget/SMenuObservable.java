package sh4j.ui.widget;

import java.util.Observable;

/**
 * Observable component of a SMenu.
 */
public class SMenuObservable extends Observable {

  /**
   * Trigger for item click in menu.
   */
  public void onItemCLick(){
    this.setChanged();
    this.notifyObservers();
  }

}
