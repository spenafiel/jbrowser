package sh4j.ui.widget;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JMenu;
import javax.swing.JMenuItem;

/**
 * Custom implementation of JMenu.
 */
public class SMenu extends JMenu {

  /**
   * Constructor, forward the message to the super class.
   *
   * @param title The title of the menu.
   */
  public SMenu(final String title) {
    super(title);
  }

  /**
   * Adds a new option to the dropdown menu.
   *
   * @param name     The visible name for the option.
   */
  public void addOption(String name, Observer suscriptor) {
    JMenuItem item = new JMenuItem(name);
    SMenuObservable obs = new SMenuObservable();
    obs.addObserver(suscriptor);
    item.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        obs.onItemCLick();
      }
    });
    this.add(item);
  }


}
