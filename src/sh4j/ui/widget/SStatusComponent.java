package sh4j.ui.widget;

import sh4j.model.browser.SProject;
import sh4j.model.command.SCountCommand;

import javax.swing.JLabel;
import javax.swing.SwingConstants;

/**
 * Abstract class for defining a Status bar component.
 */
public class SStatusComponent extends JLabel {

  private String prefix;
  private SCountCommand command;

  /**
   * Constructor initialize the default behaviour of the component.
   */
  public SStatusComponent(String prefix, SCountCommand command) {
    super();
    this.setHorizontalAlignment(SwingConstants.LEFT);
    this.prefix = prefix;
    this.command = command;
  }

  public void updateText(SProject project){
    this.setText(prefix+command.getCount(project));
  }


}
