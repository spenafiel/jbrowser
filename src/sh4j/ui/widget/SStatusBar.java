package sh4j.ui.widget;

import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JPanel;

/**
 * Definition for a status bar.
 */
public class SStatusBar extends JPanel {

  List<SStatusComponent> children;

  public SStatusBar(int width, int height) {
    super();
    this.setPreferredSize(new Dimension(width, height));
    this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
    children = new ArrayList<>();
  }

  public void addStatusComponent(SStatusComponent component) {
    this.add(component);
    this.add(Box.createRigidArea(new Dimension(16,0)));
    children.add(component);
  }

  public List<SStatusComponent> getChildren(){
    return children;
  }

}
