package sh4j.ui;

import sh4j.model.browser.SObject;
import sh4j.model.format.SSimpleTextItemFormatter;
import sh4j.model.format.STextItemFormatter;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.DefaultListCellRenderer;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.border.Border;

/**
 * Each object of this class represent a cell in the browser for instance, package cell, class cell
 * and method cell.
 *
 * @author juampi
 */
@SuppressWarnings("serial")
public class SItemRenderer extends JLabel implements ListCellRenderer<SObject> {
  protected DefaultListCellRenderer defaultRenderer = new DefaultListCellRenderer();
  private STextItemFormatter formatter;

  /**
   * creates an item renderer.
   */
  public SItemRenderer() {
    setOpaque(true);
    setBorder(BorderFactory.createLineBorder(Color.WHITE));
    formatter = new SSimpleTextItemFormatter();
  }

  /**
   * Returns a JLabel for each SObject in the JList.
   */
  public Component getListCellRendererComponent(
      JList<? extends SObject> list, SObject value, int index,
      boolean isSelected, boolean cellHasFocus) {
    setText(value.formatText(formatter));
    setFont(value.font());
    setIcon(new ImageIcon(value.icon()));
    setBackground(value.background());
    if (isSelected) {
      setBorder(BorderFactory.createLineBorder(Color.BLUE));
    } else {
      setBorder(BorderFactory.createLineBorder(Color.WHITE));
    }
    return this;
  }

  /**
   * Changes the text item formatter.
   * @param textItemFormatter The new text item formatter.
   */
  public void setFormatter(STextItemFormatter textItemFormatter){
    formatter = textItemFormatter;
  }

}
