package sh4j.ui;

import sh4j.model.browser.SClass;
import sh4j.model.browser.SFactory;
import sh4j.model.browser.SMethod;
import sh4j.model.browser.SPackage;
import sh4j.model.browser.SProject;
import sh4j.model.command.SCommand;
import sh4j.model.command.SCountClasses;
import sh4j.model.command.SCountLOC;
import sh4j.model.command.SCountWarnings;
import sh4j.model.format.SIndentTextItemFormatter;
import sh4j.model.format.SSimpleTextItemFormatter;
import sh4j.model.highlight.SHighlighter;
import sh4j.model.style.SBredStyle;
import sh4j.model.style.SDarkStyle;
import sh4j.model.style.SEclipseStyle;
import sh4j.model.style.SLineStyle;
import sh4j.model.style.SStyle;
import sh4j.ui.widget.SMenu;
import sh4j.ui.widget.SStatusBar;
import sh4j.ui.widget.SStatusComponent;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 * JBrowser
 *
 * @author juampi
 */
@SuppressWarnings({"unchecked", "serial"})
public class SFrame extends JFrame {

  private SHighlighter[] lighters;
  protected SLineStyle style = new SEclipseStyle();
  private JList<SPackage> packages;
  private JList<SClass> classes;
  private JList<SMethod> methods;
  private JEditorPane htmlPanel;
  private SMenu menuSort, menuStyle, menuView;
  private SProject project;
  private final SLineStyle[] styles = {new SEclipseStyle(), new SDarkStyle(), new SBredStyle()};
  private SStatusBar statusBar;
  private SItemRenderer classRenderer;

  /**
   * Create a browser, it will use the style and the lighters passed in the arguments.
   */
  public SFrame(SLineStyle style, SHighlighter... lighters) {
    super("CC3002 Browser");
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    setResizable(false);
    setSize(450, 750);
    build();
    this.lighters = lighters;
    this.style = style;
  }

  /**
   * Add commands to the menu bar
   */
  public void addCommands(SCommand... cmds) {
    for (SCommand c : cmds) {
      menuSort.addOption(c.name(),new Observer() {
        @Override
        public void update(final Observable observable, final Object o) {
          if (project != null) {
            c.executeOn(project, getContext());
            updateProject(project);
          }
        }
      });
    }
  }

  private void addStyleOptions() {
    for (SLineStyle s : styles) {
      menuStyle.addOption(s.toString(),new Observer() {
        @Override
        public void update(final Observable observable, final Object o) {
          style(s);
        }
      });
    }
  }

  private void buildBar() {
    JMenuBar bar = new JMenuBar();
    menuSort = new SMenu("Sort");
    bar.add(menuSort);
    menuStyle = new SMenu("Style");
    addStyleOptions();
    bar.add(menuStyle);
    menuView = new SMenu("View");
    addToggleOption();
    bar.add(menuView);
    setJMenuBar(bar);
  }

  private void buildStatusBar(JPanel panel, String direction) {
    statusBar = new SStatusBar(this.getWidth(), 16);
    panel.add(statusBar, direction);
    SStatusComponent linesOfCode = new SStatusComponent("Lines of Code: ", new SCountLOC());
    statusBar.addStatusComponent(linesOfCode);
    SStatusComponent numClasses = new SStatusComponent("Classes: ", new SCountClasses());
    statusBar.addStatusComponent(numClasses);
    SStatusComponent numWarnings = new SStatusComponent("Warnigns: ", new SCountWarnings());
    statusBar.addStatusComponent(numWarnings);
  }

  private void addToggleOption() {
    menuView.addOption("Toggle Line Numbers",new Observer() {
      @Override
      public void update(final Observable observable, final Object o) {
        toogleLineNumbers();
      }
    });
  }

  private void toogleLineNumbers() {
    style.toggleLineNumbers();
    style(style);
  }

  public void updateProject(JList list, List data) {
    list.setListData(data.toArray());
    if (data.size() > 0) {
      list.setSelectedIndex(0);
    }
  }

  private void build() {
    buildBar();
    buildPathPanel();
    methods = new JList<SMethod>();
    addOn(methods, BorderLayout.EAST);
    classes = new JList<SClass>();
    classRenderer = new SItemRenderer();
    addOn(classes, BorderLayout.CENTER, classRenderer);
    packages = new JList<SPackage>();
    addOn(packages, BorderLayout.WEST);
    JPanel bottomPanel = new JPanel();
    bottomPanel.setLayout(new BorderLayout());
    htmlPanel = buildHTMLPanel(bottomPanel, BorderLayout.NORTH);
    buildStatusBar(bottomPanel, BorderLayout.SOUTH);
    getContentPane().add(bottomPanel, BorderLayout.SOUTH);
    packages.addListSelectionListener(new ListSelectionListener() {
      @Override
      public void valueChanged(ListSelectionEvent e) {
        if (packages.getSelectedIndex() != -1) {
          updateProject(classes, packages.getSelectedValue().classes());
        }
      }
    });
    classes.addListSelectionListener(new ListSelectionListener() {
      @Override
      public void valueChanged(ListSelectionEvent e) {
        if (classes.getSelectedIndex() != -1) {
          updateProject(methods, classes.getSelectedValue().methods());
        }
      }
    });
    methods.addListSelectionListener(new ListSelectionListener() {
      @Override
      public void valueChanged(ListSelectionEvent e) {
        if (methods.getSelectedIndex() != -1) {
          SMethod method = methods.getSelectedValue();
          htmlPanel.setText(method.body().toHTML(style, lighters));
        }
      }
    });

  }

  private void addOn(JList list, String direction){
    addOn(list,direction,new SItemRenderer());
  }

  private void addOn(JList list, String direction, SItemRenderer renderer) {
    list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    list.setLayoutOrientation(JList.VERTICAL);
    list.setCellRenderer(renderer);
    JScrollPane pane = new JScrollPane(list);
    pane.setMinimumSize(new Dimension(200, 200));
    pane.setPreferredSize(new Dimension(200, 200));
    pane.setMaximumSize(new Dimension(200, 200));
    getContentPane().add(pane, direction);
  }

  private void buildPathPanel() {
    JPanel pathPanel = new JPanel();
    pathPanel.setLayout(new BorderLayout());
    final JTextField pathField = new JTextField();
    pathField.setEnabled(false);
    JButton browseButton = new JButton("Browse");
    browseButton.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        JFileChooser chooser = new JFileChooser();
        chooser.setCurrentDirectory(new java.io.File("."));
        chooser.setDialogTitle("Select a root folder");
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        chooser.setAcceptAllFileFilterUsed(false);
        if (chooser.showOpenDialog(SFrame.this) == JFileChooser.APPROVE_OPTION) {
          pathField.setText(chooser.getSelectedFile().getPath());
          try {
            project = new SFactory().create(chooser.getSelectedFile().getPath());
            updateProject(packages, project.packages());
            updateStatus(project);
            if (project.packages().size() > 0) {
              packages.setSelectedIndex(0);
            }
          } catch (IOException ex) {

          }
        }
      }
    });
    pathPanel.add(pathField, BorderLayout.CENTER);
    pathPanel.add(browseButton, BorderLayout.EAST);
    getContentPane().add(pathPanel, BorderLayout.NORTH);
  }

  private JEditorPane buildHTMLPanel(JPanel panel, String direction) {
    JEditorPane htmlPanel = new JEditorPane();
    htmlPanel.setEditable(false);
    htmlPanel.setMinimumSize(new Dimension(600, 300));
    htmlPanel.setPreferredSize(new Dimension(600, 300));
    htmlPanel.setMaximumSize(new Dimension(600, 300));
    htmlPanel.setContentType("text/html");
    panel.add(new JScrollPane(htmlPanel, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
        JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS), direction);
    return htmlPanel;
  }

  public void updateProject(SProject project) {
    SPackage pkg = packages.getSelectedValue();
    SClass cls = classes.getSelectedValue();
    SMethod method = methods.getSelectedValue();
    packages.setListData(project.packages().toArray(new SPackage[]{}));
    packages.setSelectedValue(pkg, true);
    if (pkg != null) {
      classes.setListData(pkg.classes().toArray(new SClass[]{}));
      classes.setSelectedValue(cls, true);
      if (cls != null) {
        methods.setListData(cls.methods().toArray(new SMethod[]{}));
        methods.setSelectedValue(method, true);
      }
    }
  }

  private void updateStatus(SProject project) {
    for (SStatusComponent component : statusBar.getChildren()) {
      component.updateText(project);
    }
  }

  public void style(SLineStyle style) {
    this.style = style;
    SMethod method = methods.getSelectedValue();
    htmlPanel.setText(method.body().toHTML(style, lighters));
  }

  public void setClassIndentation(boolean indent){
    if(indent){
      classRenderer.setFormatter(new SIndentTextItemFormatter());
    }
    else {
      classRenderer.setFormatter(new SSimpleTextItemFormatter());
    }
  }

  private SFrame getContext(){
    return this;
  }

}
