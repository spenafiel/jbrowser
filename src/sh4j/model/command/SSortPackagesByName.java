package sh4j.model.command;

import sh4j.model.browser.SPackage;
import sh4j.model.browser.SProject;

import java.util.List;

/**
 * Sorts the specified project by packages name.
 */
public class SSortPackagesByName extends SCommand {

  @Override
  public void executeOn(SProject project) {
    List<SPackage> packages = project.packages();
    packages.sort(new PackagesNameComparator());
  }

  @Override
  public String name() {
    return "Sort Packages by Name";
  }

}
