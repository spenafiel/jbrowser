package sh4j.model.command;

import sh4j.model.browser.SClass;

import java.util.Comparator;

/**
 * Comparator for classes names.
 */
public class ClassNameComparator implements Comparator<SClass> {

  @Override
  public int compare(SClass t0, SClass t1) {
    return t0.toString().compareTo(t1.toString());

  }
}
