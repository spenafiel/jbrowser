package sh4j.model.command;

import sh4j.model.browser.SClass;
import sh4j.model.browser.SPackage;
import sh4j.model.browser.SProject;
import sh4j.ui.SFrame;

import java.util.List;

/**
 * Sorts the specified project by class name.
 */
public class SSortClassesByName extends SCommand {

  @Override
  public void executeOn(SProject project) {
    for (SPackage pack : project.packages()) {
      List<SClass> classes = pack.classes();
      classes.sort(new ClassNameComparator());
    }
  }

  @Override
  public void executeOn(SProject project, SFrame frame) {
    executeOn(project);
    frame.setClassIndentation(false);
  }

  @Override
  public String name() {
    return "Sort Classes by Name";
  }

}
