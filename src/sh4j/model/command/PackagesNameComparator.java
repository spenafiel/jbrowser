package sh4j.model.command;

import sh4j.model.browser.SPackage;

import java.util.Comparator;

/**
 * Comparator for packages by name.
 */
public class PackagesNameComparator implements Comparator<SPackage> {
  @Override
  public int compare(SPackage t0, SPackage t1) {
    return t0.toString().compareTo(t1.toString());
  }
}
