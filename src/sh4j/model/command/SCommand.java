package sh4j.model.command;

import sh4j.model.browser.SProject;
import sh4j.ui.SFrame;

/**
 * It represent a command that could be applied to a project.
 *
 * @author juampi
 */
public abstract class SCommand {

  /**
   * Executes the command in the specified project.
   *
   * @param project The project to be executed.
   * @param frame   The parent frame instance.
   */
  public void executeOn(SProject project, SFrame frame) {
    executeOn(project);
  }

  ;

  /**
   * Executes the command in the specified project.
   *
   * @param project The project to be executed.
   */
  public abstract void executeOn(SProject project);

  /**
   * Returns the name of the command.
   *
   * @return The name of the command.
   */
  public String name() {
    return this.getClass().getName();
  }

}
