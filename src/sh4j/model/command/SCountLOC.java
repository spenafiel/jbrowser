package sh4j.model.command;

import sh4j.model.browser.SClass;
import sh4j.model.browser.SMethod;
import sh4j.model.browser.SPackage;
import sh4j.model.browser.SProject;

/**
 * Counting command for lines of codes.
 */
public class SCountLOC extends SCountCommand {
  @Override
  public void executeOn(final SProject project) {
    for (SPackage spackage : project.packages()) {
      for (SClass sclass : spackage.classes()) {
        for (SMethod method : sclass.methods()) {
          count += method.getLinesOfCode();
        }
      }
    }
  }
}
