package sh4j.model.command;

import sh4j.model.browser.SPackage;
import sh4j.model.browser.SProject;

/**
 * Counting command for counting the number of classes.
 */
public class SCountClasses extends SCountCommand {

  @Override
  public void executeOn(final SProject project) {
    for (SPackage pack : project.packages()) {
      for (int i = 0; i < pack.classes().size(); i++) {
        count++;
      }
    }
  }

}
