package sh4j.model.command;

import sh4j.model.browser.SProject;

/**
 * Abstract class for counting commands.
 */
public abstract class SCountCommand extends SCommand {

  protected int count;

  /**
   * Perform the count and return its value.
   * @param project The project to be counted.
   * @return The total count.
   */
  public int getCount(SProject project) {
    count = 0;
    executeOn(project);
    return count;
  }

}
