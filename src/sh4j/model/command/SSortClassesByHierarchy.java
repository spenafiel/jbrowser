package sh4j.model.command;

import sh4j.model.browser.SClass;
import sh4j.model.browser.SPackage;
import sh4j.model.browser.SProject;
import sh4j.ui.SFrame;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

/**
 * Sorts the specified project by class hierarchy.
 */
public class SSortClassesByHierarchy extends SCommand {

  @Override
  public void executeOn(SProject project, SFrame frame) {
    executeOn(project);
    frame.setClassIndentation(true);
  }

  @Override
  public void executeOn(SProject project) {
    for (SPackage pack : project.packages()) {
      List<SClass> classes = pack.classes();
      sortClasses(classes);
    }
  }

  private void sortClasses(List<SClass> classes) {
    Map<String, List<SClass>> map = new Hashtable<>();
    map.put("Object", new ArrayList<>());

    for (SClass sclass : classes) {
      String superClass = sclass.superClass();
      if (map.containsKey(superClass)) {
        map.get(superClass).add(sclass);
      } else {
        map.put(superClass, new ArrayList<>());
        map.get(superClass).add(sclass);
      }
    }

    for (List<SClass> list : map.values()) {
      list.sort(new ClassNameComparator());
    }

    List<SClass> orderedList = getClassHierarchy(map, "Object", 0);
    classes.clear();
    classes.addAll(orderedList);
  }

  private List<SClass> getClassHierarchy(Map<String, List<SClass>> map, String object, int level) {
    List<SClass> children = map.get(object);
    List<SClass> out = new ArrayList<>();
    if (children == null) {
      return out;
    }
    for (SClass sclass : children) {
      sclass.setHierarchyLevel(level);
      out.add(sclass);
      out.addAll(getClassHierarchy(map, sclass.toString(), level + 1));
    }
    return out;
  }

  @Override
  public String name() {
    return "Sort Classes by Hierarchy";
  }

}
