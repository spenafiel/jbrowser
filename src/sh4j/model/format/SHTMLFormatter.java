package sh4j.model.format;

import sh4j.model.highlight.SDummy;
import sh4j.model.highlight.SHighlighter;
import sh4j.model.style.SLineStyle;
import sh4j.parser.model.SBlock;
import sh4j.parser.model.SText;

/**
 * HTML text formatter.
 */
public class SHTMLFormatter implements SFormatter {
  private final StringBuffer buffer;
  private int level;
  private int line;
  private final SLineStyle style;
  private final SHighlighter[] highlighters;

  /**
   * Creates a new HTML formatter according to the style and the highlighters.
   */
  public SHTMLFormatter(SLineStyle style, SHighlighter... hs) {
    this.style = style;
    highlighters = hs;
    buffer = new StringBuffer();
    line = 1;
  }

  private SHighlighter lookup(String text) {
    for (SHighlighter h : highlighters) {
      if (h.needsHighLight(text)) {
        return h;
      }
    }
    return new SDummy();
  }

  @Override
  public void styledWord(String word) {
    buffer.append(lookup(word).highlight(word, style));
  }

  @Override
  public void styledChar(char character) {
    buffer.append(lookup(character + "").highlight(character + "", style));
  }

  @Override
  public void styledSpace() {
    buffer.append(' ');
  }

  @Override
  public void styledCR() {
    buffer.append("\n");
    buffer.append(style.formatLineOfCode(line));
    indent();
    line++;
  }

  @Override
  public void styledBlock(SBlock block) {
    level++;
    for (SText text : block.texts()) {
      text.export(this);
    }
    level--;
  }

  /**
   * Adds indentation to the current line.
   */
  public void indent() {
    for (int i = 0; i < level; i++) {
      buffer.append("  ");
    }
  }

  @Override
  public String formattedText() {
    return style.formatBody(buffer.toString());
  }

  /*private String formatLineOfCode(int line) {
    String num = line + " ";
    while (num.length() < 4) {
      num = " " + num;
    }
    return tag("span", num, "background:#f1f0f0;");
  }*/

  /**
   * Returns a text wrapped by a tag and a certain style.
   */
  public static String tag(String name, String content, String style) {
    return "<" + name + " style='" + style + "'>" + content + "</" + name + ">";
  }
}
