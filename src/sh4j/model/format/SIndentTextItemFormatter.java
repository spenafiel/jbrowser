package sh4j.model.format;

/**
 * Text item formatter with indentation.
 */
public class SIndentTextItemFormatter implements STextItemFormatter {

  private int level = 0;

  @Override
  public String formatText(final String text) {
    StringBuilder buffer = new StringBuilder();
    for (int i = 0; i < level; i++) {
      buffer.append("   ");
    }
    buffer.append(text);
    return buffer.toString();
  }

  @Override
  public void setLevel(int lev) {
    level = lev;
  }

}
