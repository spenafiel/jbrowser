package sh4j.model.format;

import sh4j.parser.model.SBlock;

/**
 * Defines a text formatter.
 */
public interface SFormatter {

  /**
   * Styles a word.
   * @param word the word to be styled.
   */
  public void styledWord(String word);

  /**
   * Styles a character.
   * @param c the character to be styled.
   */
  public void styledChar(char character);

  /**
   * Styles a space.
   */
  public void styledSpace();

  /**
   * Styles a carriage return.
   */
  public void styledCR();

  /**
   * Styles a block of code.
   * @param b the block of code to be styled.
   */
  public void styledBlock(SBlock block);

  /**
   * Returns the formatted string.
   * @return The formatted string.
   */
  public String formattedText();
}
