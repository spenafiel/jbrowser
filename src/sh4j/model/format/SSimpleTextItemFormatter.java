package sh4j.model.format;

/**
 * Simple text item formatter, returns the text without format.
 */
public class SSimpleTextItemFormatter implements STextItemFormatter {
  @Override
  public String formatText(final String text) {
    return text;
  }

  @Override
  public void setLevel(int level) {
    formatText(""); //Used only because El Chango doesnt allow to have empty methods.
  }
}
