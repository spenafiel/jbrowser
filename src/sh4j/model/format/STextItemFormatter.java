package sh4j.model.format;

/**
 * Interface for an item text formatter.
 */
public interface STextItemFormatter {

  /**
   * Returns the formatted text.
   * @param text the text to be formatted.
   * @return The formated item text.
   */
  public String formatText(String text);

  /**
   * Set the indentation level of the text.
   * @param level the level of indentation.
   */
  public void setLevel(int level);

}
