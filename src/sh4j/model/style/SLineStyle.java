package sh4j.model.style;

import static sh4j.model.format.SHTMLFormatter.tag;

/**
 * Definition of a line numbered style.
 */
public abstract class SLineStyle implements SStyle {

  protected boolean formatLineNumber = true;

  /**
   * Adds format of line of code if it is enabled.
   * @param line The number of the line.
   * @return The HTML string representing a number of line.
   */
  public String formatLineOfCode(int line) {
    if (formatLineNumber) {
      String num = line + " ";
      while (num.length() < 4) {
        num = " " + num;
      }
      return tag("span", num, "background:#f1f0f0;");
    } else {
      return "";
    }
  }

  /**
   * Toggles the line numbers visibility.
   */
  public void toggleLineNumbers() {
    formatLineNumber = !formatLineNumber;
  }
}
