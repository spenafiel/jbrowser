package sh4j.model.style;

/**
 * Abstract class for a Style.
 */
public interface SStyle {

  public abstract String toString();

  public abstract String formatClassName(String text);

  public abstract String formatCurlyBracket(String text);

  public abstract String formatKeyWord(String text);

  public abstract String formatPseudoVariable(String text);

  public abstract String formatSemiColon(String text);

  public abstract String formatString(String text);

  public abstract String formatMainClass(String text);

  public abstract String formatModifier(String text);

  public abstract String formatBody(String text);

}
