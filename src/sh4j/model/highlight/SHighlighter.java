package sh4j.model.highlight;

import sh4j.model.style.SStyle;

/**
 * Interface for a highlighter.
 */
public interface SHighlighter {
  /**
   * Returns true if the text needs to be highlighted.
   *
   * @param text the text to be highlighted.
   * @return true if the text needs to be highlighted by the highlighter.
   */
  public boolean needsHighLight(String text);

  /**
   * Returns the styled text.
   *
   * @param text  The text to be highlighted.
   * @param style The style to be applied.
   * @return The string representing the text highlighted.
   */
  public String highlight(String text, SStyle style);
}
