package sh4j.model.browser;

import sh4j.model.format.STextItemFormatter;

import java.awt.Color;
import java.awt.Font;

/**
 * Represent a Java project entity.
 */
public abstract class SObject {

  /**
   * Defines the background color for the item renderer.
   *
   * @return the item background color.
   */
  public abstract Color background();

  /**
   * Defines the icon to be show by the item renderer.
   *
   * @return The string representing the path of the icon.
   */
  public abstract String icon();

  /**
   * Defines the font face to be used by the item renderer.
   *
   * @return The font for the item.
   */
  public abstract Font font();

  public String formatText(STextItemFormatter textItemFormatter) {
    return this.toString();
  }
}
