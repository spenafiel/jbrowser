package sh4j.model.browser;

import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;
import java.util.List;

/**
 * Represent a Java Project.
 */
public class SProject extends SObject {
  private final List<SPackage> packages;

  /**
   * Creates a new empty project.
   */
  public SProject() {
    packages = new ArrayList<SPackage>();
  }

  /**
   * Adds a package to the project.
   * @param pack The package to be added.
   */
  public void addPackage(SPackage pack) {
    packages.add(pack);
  }

  /**
   * Returns the list of packages in the project.
   * @return The list of packages in the project.
   */
  public List<SPackage> packages() {
    return packages;
  }

  /**
   * Search a package in the project by name.
   * @param pkgName The name of the package.
   * @return The package or null if there is not.
   */
  public SPackage get(String pkgName) {
    for (SPackage pkg : packages) {
      if (pkg.toString().equals(pkgName)) {
        return pkg;
      }
    }
    return null;
  }

  @Override
  public Color background() {
    return Color.WHITE;
  }

  @Override
  public String icon() {
    return null;
  }

  @Override
  public Font font() {
    return null;
  }
}
