package sh4j.model.browser;

import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;
import java.util.List;

/**
 * Represent a Java package.
 */
public class SPackage extends SObject {

  private final String name;
  private final List<SClass> classes;

  /**
   * Creates a new empty package.
   *
   * @param name The name of the package.
   */
  public SPackage(String name) {
    classes = new ArrayList<SClass>();
    this.name = name;
  }

  /**
   * Add a class to the package.
   *
   * @param cls The class to be added.
   */
  public void addClass(SClass cls) {
    classes.add(cls);
  }

  /**
   * Returns the list of the classes in the package.
   *
   * @return The list of the classes in the package.
   */
  public List<SClass> classes() {
    return classes;
  }

  public String toString() {
    return name;
  }

  @Override
  public Color background() {
    return Color.WHITE;
  }

  @Override
  public String icon() {
    if (classes.isEmpty()) {
      return "./resources/pack_empty_co.gif";
    } else {
      return "./resources/package_mode.gif";
    }
  }

  @Override
  public Font font() {
    return new Font("Helvetica", Font.PLAIN, 12);
  }
}
