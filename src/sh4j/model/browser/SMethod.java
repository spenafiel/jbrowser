package sh4j.model.browser;

import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.Modifier;
import sh4j.parser.model.SBlock;

import java.awt.Color;
import java.awt.Font;

/**
 * Represent a method of a class.
 */
public class SMethod extends SObject {
  private final MethodDeclaration declaration;
  private final SBlock body;

  /**
   * Creates a new method based in a declaration and a block of code.
   */
  public SMethod(MethodDeclaration node, SBlock body) {
    declaration = node;
    this.body = body;
  }

  /**
   * Returns the modifier of the method.
   *
   * @return The modifier of the method.
   */
  public String modifier() {
    for (Object obj : declaration.modifiers()) {
      if (obj instanceof Modifier) {
        Modifier modifier = (Modifier) obj;
        return modifier.getKeyword().toString();
      }
    }
    return "default";
  }

  /**
   * Returns the method's name.
   *
   * @return The method's name.
   */
  public String name() {
    return declaration.getName().getIdentifier();
  }

  /**
   * Get te block of code of the method's definition.
   *
   * @return The block of code which defines the method.
   */
  public SBlock body() {
    return body;
  }

  public String toString() {
    return name();
  }

  @Override
  public Color background() {
    int lines = getLinesOfCode();
    if (lines > 50) {
      return Color.RED;
    } else if (lines > 30) {
      return Color.YELLOW;
    } else {
      return Color.WHITE;
    }
  }

  /**
   * Returns the number of lines of code of the method.
   *
   * @return Lines of code of the method's body.
   */
  public int getLinesOfCode() {
    return body.getLines();
  }

  @Override
  public String icon() {
    return "./resources/" + this.modifier() + "_co.gif";
  }

  @Override
  public Font font() {
    return new Font("Helvetica", Font.PLAIN, 12);
  }

}
