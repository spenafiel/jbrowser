package sh4j.model.browser;

import org.eclipse.jdt.core.dom.TypeDeclaration;
import sh4j.model.format.STextItemFormatter;

import java.awt.Color;
import java.awt.Font;
import java.util.List;

/**
 * Represent a Java Class.
 */
public class SClass extends SObject {
  private final TypeDeclaration declaration;
  private final List<SMethod> methods;
  private int hierarchyLevel;

  /**
   * Creates a new Class.
   *
   * @param td Defines the type of the class.
   * @param ms The list of methods in the class.
   */
  public SClass(TypeDeclaration td, List<SMethod> ms) {
    declaration = td;
    methods = ms;
    hierarchyLevel = 0;
  }

  /**
   * The list of methods defined in the class.
   *
   * @return The list of methods in the class.
   */
  public List<SMethod> methods() {
    return methods;
  }

  /**
   * Returns the class name.
   *
   * @return The class name.
   */
  public String className() {
    return declaration.getName().getIdentifier();
  }

  /**
   * Returns true if the class represented is an interface.
   *
   * @return True if it is an Interface.
   */
  public boolean isInterface() {
    return declaration.isInterface();
  }

  /**
   * Returns the name of the super class. (Object included).
   *
   * @return The name of the super class.
   */
  public String superClass() {
    if (declaration.getSuperclassType() == null) {
      return "Object";
    }
    return declaration.getSuperclassType().toString();
  }

  public String toString() {
    return className();
  }

  @Override
  public Color background() {
    return Color.WHITE;
  }

  @Override
  public String icon() {
    if (this.isInterface()) {
      return "./resources/int_obj.gif";
    } else {
      return "./resources/class_obj.gif";
    }
  }

  @Override
  public Font font() {
    if (this.isInterface()) {
      return new Font("Helvetica", Font.ITALIC, 12);
    } else {
      return new Font("Helvetica", Font.PLAIN, 12);
    }
  }

  @Override
  public String formatText(STextItemFormatter formatter) {
    formatter.setLevel(hierarchyLevel);
    return formatter.formatText(this.toString());
  }

  public void setHierarchyLevel(final int hierarchyLevel) {
    this.hierarchyLevel = hierarchyLevel;
  }
}